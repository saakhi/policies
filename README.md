SAAKHI PROJECT
==============

Saakhi is a platform that aims to transform research into a collaborative community process
with expertise sharing, open publishing, open peer reviews and public accountability. This
repository contain all policies that guide the development and deployment of the platform.
You can access policy documents with the links below:

1. [General Policy Statement](general.md)
